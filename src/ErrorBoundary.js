import React, { Fragment } from "react";
import { Row, Card, CardTitle, Button, Col } from "reactstrap";

import IntlMessages from "./helpers/IntlMessages";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// import * as action from "views/Stores/Analytics/actions";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error: error, errorInfo: errorInfo });
  }

  // componentDidUpdate() {
  //   if (this.state.errorInfo) {
  //     this.props.actions.pushErrorPage({ description: this.state.errorInfo.componentStack });
  //     this.props.actions.pushErrorPage({ description: this.state.error.toString() });
  //   }
  // }

  render() {
    if (this.state.errorInfo) {
      // Error path
      return (
        <Fragment>
          <div className="fixed-background" />
          <div className="container">
            <Row className="h-100 mx-auto my-auto">
              <Col xxs="12" md="10" className="mx-auto my-auto">
                <Card className="auth-card">
                  <div className="position-relative image-side ">
                    <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
                    <p className="white mb-0">Yes, it is indeed!</p>
                  </div>
                  <div className="form-side">
                    <span className="logo-single" />
                    <CardTitle className="mb-4">
                      <IntlMessages id="pages.error-title" />
                    </CardTitle>
                    <Button
                      href="/"
                      color="primary"
                      className="btn-shadow"
                      size="lg"
                    >
                      <IntlMessages id="pages.go-back-home" />
                    </Button>
                  </div>
                </Card>
              </Col>
            </Row>
          </div>
        </Fragment>
      );
    }
    // Normally, just render children
    return <Fragment>{this.props.children}</Fragment>;
  }
}

function mapDispatchToProps(dispatch) {
  const actions = {};
  return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(null, mapDispatchToProps)(ErrorBoundary);
