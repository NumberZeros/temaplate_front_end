import React, { Component, Suspense, lazy, Fragment } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import { HashRouter } from "react-router-dom";
// import Header from "../layout/header";
// import Footer from "../layout/footer";

const Header = lazy(() => import("../layout/header"));
const Footer = lazy(() => import("../layout/footer"));
const Homepages = lazy(() => import("../views/Homepages/components/Container"));
const Products = lazy(() => import("../views/Products/components/Container"));
const ProductDetails = lazy(() =>
  import("../views/Detail/components/Container")
);
const Error404 = lazy(() => import("../pages/404error"));
const About = lazy(() => import("../pages/about-us"));
const Blog = lazy(() => import("../pages/blog-detail"));
const BlogDetails = lazy(() => import("../pages/blog"));
const List = lazy(() => import("../pages/list"));
const Grid = lazy(() => import("../pages/grid"));
const Login = lazy(() => import("../pages/login"));
const MultipleAddresses = lazy(() => import("../pages/multiple-addresses"));
const ShoppingCart = lazy(() => import("../pages/shopping-cart"));
const Wishlist = lazy(() => import("../pages/wishlist"));
const CheckoutMethod = lazy(() => import("../pages/checkout-method"));
const Checkout = lazy(() => import("../pages/checkout"));
const ContactUs = lazy(() => import("../pages/contact-us"));
const Dashboard = lazy(() => import("../pages/dashboard"));
const Newsletter = lazy(() => import("../pages/newsletter"));
const QuickView = lazy(() => import("../pages/quickview"));

class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <Fragment>
        <Header />
        <Suspense fallback={<div className="loading" />}>
          <HashRouter>
            <Switch>
              <Route path={`/404error`} component={Error404} />
              <Route path={`/about-us`} component={About} />
              <Route path={`/blog-detail`} component={Blog} />
              <Route path={`/blog`} component={BlogDetails} />
              <Route path={`/list`} component={List} />
              <Route path={`/grid`} component={Grid} />
              <Route path={`/login`} component={Login} />
              <Route
                path={`/multiple-addresses`}
                component={MultipleAddresses}
              />
              <Route path={`/product-details/:id`} component={ProductDetails} />
              <Route path={`/shopping-cart`} component={ShoppingCart} />
              <Route path={`/wishlist`} component={Wishlist} />
              <Route path={`/checkout-method`} component={CheckoutMethod} />
              <Route path={`/checkout`} component={Checkout} />
              <Route path={`/contact-us`} component={ContactUs} />
              <Route path={`/dashboard`} component={Dashboard} />
              <Route path={`/newsletter`} component={Newsletter} />
              <Route path={`/quickview`} component={QuickView} />
              <Route path={`/san-pham/:id`} component={Products} />
              <Route path={`/`} component={Homepages} />
              <Redirect to="/error" />
            </Switch>
          </HashRouter>
        </Suspense>
        <Footer />
      </Fragment>
    );
  }
}

export default withRouter(App);
