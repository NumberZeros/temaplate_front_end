import React, { Component, Suspense, lazy } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Switch, Route, withRouter } from "react-router-dom";
import { IntlProvider } from "react-intl";
import ErrorBoundary from "./ErrorBoundary";
import Loading from "./theme/common/Loading";
import { getSubdomain } from "./services/api";

const Router = lazy(() => import("./router"));

class App extends Component {
  componentDidMount() {
    getSubdomain();
  }

  render() {
    const { locale } = this.props;

    return (
      <IntlProvider locale={locale}>
        <ErrorBoundary>
          <BrowserRouter>
            <Suspense fallback={<Loading />}>
              <Switch>
                <Route path={`/`} component={Router} />
              </Switch>
            </Suspense>
          </BrowserRouter>
        </ErrorBoundary>
      </IntlProvider>
    );
  }
}

const mapStateToProps = ({ settings }) => {
  const { locale } = settings;
  return { locale };
};
const mapActionsToProps = {};

export default withRouter(connect(mapStateToProps, mapActionsToProps)(App));
