import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import Homepages, { name as nameOfHomepages } from "../views/Homepages";
import Products, { name as nameOfProducts } from "../views/Products";
import Detail, { name as nameOfDetail } from "../views/Detail";

import settings from "./settings/reducer";

///////////////////////////////////////////////////////////////////////////////////////////////

const staticReducers = {
  settings,
  [nameOfHomepages]: Homepages,
  [nameOfProducts]: Products,
  [nameOfDetail]: Detail,
};

export default (history, asyncReducers) =>
  combineReducers({
    ...staticReducers,
    ...asyncReducers,
    router: connectRouter(history),
  });
