/**
 * @file init sagas
 */

import { all } from "redux-saga/effects";
import { sagas as Homepages } from "../views/Homepages";
import { sagas as Products } from "../views/Products";
import { sagas as Detail } from "../views/Detail";

////////////////////////////////////////////////////////////////////////////////

export const defaultSagaLists = [Homepages, Products, Detail];

export default function* rootSaga() {
  yield all(defaultSagaLists.map((saga) => saga()));
}
