import { createStore, applyMiddleware, compose } from "redux";
import { createLogger } from "redux-logger";
import { routerMiddleware, connectRouter } from "connected-react-router";
import { createBrowserHistory, createMemoryHistory } from "history";
import createSagaMiddleware from "redux-saga";
import makeRootReducer from "./reducers";
import sagas from "./sagas";

// A nice helper to tell us if we're on the server
const isServer = !(
  typeof window !== "undefined" &&
  window.document &&
  window.document.createElement
);

export const history = isServer
  ? createMemoryHistory({ initialEntries: ["/"] })
  : createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
const middleware = routerMiddleware(history);
const loggerMiddleware = createLogger({ predicate: () => process.env.NODE_ENV === "development" });
const middlewares = [sagaMiddleware, middleware, loggerMiddleware];

// runSaga is middleware.run function
// rootSaga is a your root saga for static saagas
function createSagaInjector(runSaga, rootSaga) {
  // Create a dictionary to keep track of injected sagas
  const injectedSagas = new Map();
  const isInjected = key => injectedSagas.has(key);
  const injectSaga = (key, saga) => {
    // We won't run saga if it is already injected
    if (isInjected(key)) return;
    // Sagas return task when they executed, which can be used
    // to cancel them
    const task = runSaga(saga);
    // Save the task if we want to cancel it in the future
    injectedSagas.set(key, task);
  };
  // Inject the root saga as it a staticlly loaded file,
  injectSaga("root", rootSaga);

  return injectSaga;
}

export function configureStore(initialState) {
  const store = createStore(
    connectRouter(history)(makeRootReducer(history)),
    initialState,
    compose(applyMiddleware(...middlewares))
  );

  store.asyncReducers = {};
  // Create an inject reducer function
  // This function adds the async reducer, and creates a new combined reducer
  store.injectReducer = (key, asyncReducer) => {
    store.asyncReducers[key] = asyncReducer;
    store.replaceReducer(makeRootReducer(history, store.asyncReducers));
  };
  store.injectSaga = createSagaInjector(sagaMiddleware.run, sagas);

  if (module.hot) {
    module.hot.accept("./reducers", () => {
      const nextRootReducer = require("./reducers");
      store.replaceReducer(nextRootReducer);
    });
  }

  sagaMiddleware.run(sagas);

  return { store, history };
}
