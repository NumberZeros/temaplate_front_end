export const stores = [
  {
    id: "dashboard",
    icon: "iconsminds-dashboard",
    label: "menu.dashboard",
    to: "/app/stores/dashboard"
  },
  {
    id: "pos",
    icon: "iconsminds-cash-register-2",
    label: "menu.pos",
    to: "/pos/cashier",
    newWindow: true
  },
  {
    id: "todoList",
    icon: "iconsminds-check",
    label: "menu.todoList",
    to: "/app/stores/todoList"
  },
  {
    id: "merchant",
    icon: "iconsminds-shop-2",
    label: "menu.merchant",
    to: "#",
    subs: [
      {
        id: "menu-merchant",
        label: "menu.merchant",
        to: "#",
        subs: [
          {
            id: "brands",
            icon: "iconsminds-shop-2",
            label: "menu.brands",
            to: "/app/stores/brands"
          },
          {
            id: "shop",
            icon: "iconsminds-shop",
            label: "menu.stores",
            to: "/app/stores/stores"
          }
        ]
      },
      {
        id: "marketing",
        label: "menu.marketing",
        to: "#",
        subs: [
          {
            id: "banners",
            icon: "iconsminds-digital-drawing",
            label: "menu.banners",
            to: "/app/stores/banners"
          },
          {
            id: "emailMarketings",
            icon: "iconsminds-mail-gallery",
            label: "menu.email-marketings",
            to: "/app/stores/email-marketings"
          },
          {
            id: "event",
            icon: "iconsminds-target",
            label: "menu.events",
            to: "/app/stores/events"
          },
          {
            id: "voucher",
            icon: "iconsminds-qr-code",
            label: "menu.vouchers",
            to: "/app/stores/vouchers"
          }
        ]
      },
      {
        id: "menu-devicesManager",
        label: "menu.devicesManager",
        to: "#",
        subs: [
          {
            id: "orders",
            icon: "iconsminds-control",
            label: "menu.catalogs",
            to: "/app/stores/catalogs"
          }
        ]
      },
      {
        id: "nlp",
        icon: "iconsminds-voicemail",
        label: "menu.nlp",
        to: "#",
        subs: [
          {
            id: "tags",
            icon: "iconsminds-speach-bubble-asking",
            label: "menu.tags",
            to: "/app/stores/tags"
          },
          {
            id: "customizedResponses",
            icon: "iconsminds-speach-bubble-comic-2",
            label: "menu.customizedResponses",
            to: "/app/stores/customizedResponses"
          }
        ]
      }
    ]
  },
  {
    id: "crm",
    icon: "iconsminds-business-man-woman",
    label: "menu.crm",
    to: "#",
    subs: [
      {
        id: "customers",
        icon: "iconsminds-id-card",
        label: "menu.customers",
        to: "/app/stores/customers"
      },
      {
        id: "suppliers",
        icon: "iconsminds-shop",
        label: "menu.suppliers",
        to: "/app/stores/suppliers"
      }
    ]
  },
  {
    id: "shop",
    icon: "iconsminds-shop-4",
    label: "menu.shop",
    to: "#",
    subs: [
      {
        id: "menu-orders",
        label: "menu.orders",
        to: "#",
        subs: [
          {
            id: "orders",
            icon: "simple-icon-bag",
            label: "menu.orders",
            to: "/app/stores/orders"
          }
        ]
      },
      {
        id: "menu-accountants",
        label: "menu.accountants",
        to: "#",
        subs: [
          {
            id: "accountants-cashbook",
            icon: "iconsminds-books",
            label: "menu.cashbook",
            to: "/app/stores/accountants/cashbook"
          },
          {
            id: "accountants-receipt",
            icon: "iconsminds-receipt-4",
            label: "menu.receipt",
            to: "/app/stores/accountants/receipt"
          },
          {
            id: "accountants-payment",
            icon: "iconsminds-checkout",
            label: "menu.payment",
            to: "/app/stores/accountants/payment"
          }
        ]
      },
      {
        id: "menu-goods",
        label: "menu.goods",
        to: "#",
        subs: [
          {
            id: "product",
            icon: "iconsminds-hamburger",
            label: "menu.product",
            to: "/app/stores/product"
          },
          {
            id: "productcategory",
            icon: "iconsminds-monitoring",
            label: "menu.productcatergory",
            to: "/app/stores/productcategories"
          },
          {
            id: "productAddOn",
            icon: "iconsminds-tee-mug",
            label: "menu.productAddOn",
            to: "/app/stores/product-addon"
          }
        ]
      }
    ]
  },
  {
    id: "container",
    icon: "iconsminds-data-center",
    label: "menu.container",
    to: "#",
    subs: [
      {
        id: "materials",
        icon: "iconsminds-box-close",
        label: "menu.materials",
        to: "/app/stores/materials"
      },
      {
        id: "warehouse",
        icon: "simple-icon-social-dropbox",
        label: "menu.warehouse",
        to: "/app/stores/warehouse"
      }
    ]
  },
  {
    id: "notification",
    icon: "iconsminds-megaphone",
    label: "menu.notification",
    to: "/app/stores/notifications"
  },
  // {
  //   id: "settings",
  //   icon: "simple-icon-settings",
  //   label: "menu.settings",
  //   to: "/app/stores/info"
  // }
];

export const employees = [
  {
    id: "dashboard-employees",
    icon: "iconsminds-dashboard",
    label: "menu.dashboard",
    to: "/app/employees/dashboard"
  },
  {
    id: "managers",
    icon: "iconsminds-user",
    label: "menu.employeesManager",
    to: "/app/employees/managers"
  },
  {
    id: "timeTable",
    icon: "iconsminds-alarm-clock-2",
    label: "menu.time-tables",
    to: "/app/employees/time-tables"
  }
];

export const analytics = [
  {
    id: "sales",
    icon: "iconsminds-dashboard",
    label: "menu.analytics.sales",
    to: "/app/analytics/sales"
  },
  {
    id: "employees",
    icon: "iconsminds-user",
    label: "menu.analytics.employees",
    to: "/app/analytics/employees"
  },
  {
    id: "managers",
    icon: "iconsminds-user",
    label: "menu.analytics.wiki",
    to: "/app/analytics/wiki"
  }
];

export default {
  stores,
  analytics,
  employees
};
