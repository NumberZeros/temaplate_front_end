import React, { Fragment } from "react";

export default function Loading() {
  return (
    <Fragment>
      <div className="lds-ripple">
        <div></div>
        <div></div>
      </div>
    </Fragment>
  );
}
