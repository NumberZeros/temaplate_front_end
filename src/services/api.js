import axios from "axios";
import _ from "lodash";
import { history } from "../AppRenderer";
import config from "../config";
import {
  save,
  saveStore,
  getStore,
  get,
  clearEverything
} from "../services/localStoredService";

export const getSubdomain = () => {
  let hostname = window.location.hostname.toLowerCase();
  hostname = hostname.replace("-staging", "");
  const hostnameSections = hostname.split(".");
  const first = hostnameSections[0];
  saveStore("alias", first);
};

export const refresh = (requestData, refreshToken) => {
  return axios({
    method: "POST",
    url: `${config.apiBaseURL}/users/refreshToken`,
    headers: {
      authorization: refreshToken,
      "Content-Type": "application/json"
    }
  })
    .then(res => {
      const { accessToken, refreshToken, userInfo } = res.data.data.payload;
      save("accessToken", accessToken);
      save("refreshToken", refreshToken);
      save("userInfo", userInfo);
      const { endpoint, method, data, headerInput } = requestData;
      return request(endpoint, method, data, headerInput, accessToken);
    })
    .catch(err => {
      clearEverything();
      return err;
    });
};

export const handerError = (error, requestData) => {
  return new Promise(async (resolve, reject) => {
    const status = _.get(error, "response.status");
    const refreshToken = get("refreshToken");
    const message = _.get(error, "response.data.message");
    if (status < 500 && status >= 400) {
      if (status === 404) {
        return history.push("/404");
      } else if (status === 401) {
        if (refreshToken)
          return resolve(await refresh(requestData, refreshToken));
        history.push("/user");
        window.location.reload();

        return;
      } else if (status === 403) {
        clearEverything();
      } else {
        return reject(message);
      }
    } else {
      return reject(message);
    }
  });
};

export const request = (
  endpoint,
  method,
  data,
  headerInput,
  accessToken = null
) => {
  return new Promise((resolve, reject) => {
    getSubdomain();
    const getHeaders = input => {
      const token = accessToken ? accessToken : get("accessToken");
      const header = {
        authorization: token,
        "Content-Type": "application/json",
        ...input
      };
      return header;
    };
    const alias = getStore("alias");
    const options = {
      method,
      url: endpoint.replace("/pages/v1", `/pages/v1/${alias}`),
      headers: getHeaders(headerInput),
      data: method !== "GET" ? data : null,
      params: method === "GET" ? data : null
    };
    // Store event of user

    return axios(options)
      .then(res => resolve(res.data))
      .catch(async error => {
        try {
          return resolve(
            await handerError(error, { endpoint, method, data, headerInput })
          );
        } catch (err) {
          return reject(err);
        }
      });
  });
};
