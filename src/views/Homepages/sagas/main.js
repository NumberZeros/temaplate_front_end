import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "../../../apis/homepages";
import { takeAction } from "../../../services/forkActionSagas";

export function* handleGetBanners(action) {
  try {
    const res = yield call(API.getBanners);
    yield put(actions.getBannersSuccess(res.data));
  } catch (err) {
    yield put(actions.getBannersFail(err));
  }
}

export function* handleGetProductCategories(action) {
  try {
    const res = yield call(API.getProductCategories);
    yield put(actions.getProductCategoriesSuccess(res.data));
  } catch (err) {
    yield put(actions.getProductCategoriesFail(err));
  }
}

export function* handleGetBestSellers(action) {
  try {
    const res = yield call(API.getBestSellers);
    console.table(res.data);
    yield put(actions.getBestSellersSuccess(res.data));
  } catch (err) {
    yield put(actions.getBestSelelrsFail(err));
  }
}
export function* getBanners() {
  yield takeAction(actions.getBanners, handleGetBanners);
}

export function* getProductCategories() {
  yield takeAction(actions.getProductCategories, handleGetProductCategories);
}

export function* getBestSellers() {
  yield takeAction(actions.getBestSellers, handleGetBestSellers);
}

export default [getBanners, getProductCategories, getBestSellers];
