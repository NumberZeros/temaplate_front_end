/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const getBanners = createAction("HOMEPAGE/GET_BANNERS");
export const getBannersFail = createAction("HOMEPAGE/GET_BANNERS_FAIL");
export const getBannersSuccess = createAction("HOMEPAGE/GET_BANNERS_SUCCESS");
export const getProductCategories = createAction(
  "HOMEPAGE/GET_PRODUCT_CATEGORIES"
);
export const getProductCategoriesFail = createAction(
  "HOMEPAGE/GET_PRODUCT_CATEGORIES_FAIL"
);
export const getProductCategoriesSuccess = createAction(
  "HOMEPAGE/GET_PRODUCT_CATEGORIES_SUCCESS"
);
export const getBestSellers = createAction("HOMEPAGE/GET_BEST");
export const getBestSellersSuccess = createAction("HOMEPAGE/GET_BEST_SUCCESS");
export const getBestSelelrsFail = createAction("HOMEPAGE/GET_BEST_FAIL");
