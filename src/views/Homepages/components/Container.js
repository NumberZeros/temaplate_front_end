import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Homepages from "./Homepages";
import Loading from "../../../theme/common/Loading";
import { isEmpty } from "lodash";

class ProductContainer extends Component {
  componentDidMount() {
    const { topCategories, actions, banners, bestSellers } = this.props;
    if (isEmpty(topCategories)) {
      actions.getProductCategories();
    }
    if (isEmpty(banners)) {
      actions.getBanners();
    }
    actions.getBestSellers();
  }
  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <Loading />}
        <Homepages {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name],
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action,
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProductContainer)
);
