import React, { Component } from "react";
import { Link } from "react-router-dom";
import TopCategories from "./TopCategories";
import BestSeller from "./BestSeller";
import BrandCarousel from "../../../element/brand-slider-carousel";
import TestimonialCarousel from "./TestimonialCarousel";
import BannerSlider from "./BannerSlider";
import Blog from "./Blog";
import { Row } from "reactstrap";
import DealOfWeek from "./DealOfWeek";

class Index extends Component {
  componentDidMount() {
    window.sliderr();
    window.commonjs();
  }
  render() {
    return (
      <>
        <div className="content">
          <div id="thmg-slider-slideshow" class="thmg-slider-slideshow">
            <div class="container">
              <div
                id="thm_slider_wrapper"
                class="thm_slider_wrapper fullwidthbanner-container"
              >
                <div id="thm-rev-slider" class="rev_slider fullwidthabanner">
                  <BannerSlider banners={this.props.banners} />
                </div>
              </div>
            </div>
          </div>

          <div className="top-cate">
            <div className="featured-pro container">
              <Row>
                <div className="col-lg-12">
                  <div className="slider-items-products">
                    <TopCategories topCategories={this.props.topCategories} />
                  </div>
                </div>
              </Row>
            </div>
          </div>

          <div id="top">
            <div className="container">
              <Row>
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  {" "}
                  <Link to={" "} data-scroll-goto="1">
                    {" "}
                    <img
                      src="/assets/img/banner-img1.jpg"
                      alt="promotion-banner1"
                    />{" "}
                  </Link>{" "}
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  {" "}
                  <Link to={" "} data-scroll-goto="2">
                    {" "}
                    <img
                      src="/assets/img/banner-img2.jpg"
                      alt="promotion-banner2"
                    />{" "}
                  </Link>{" "}
                </div>
              </Row>
            </div>
          </div>

          {/* <!-- best Pro Slider --> */}
          <section className=" wow bounceInUp animated">
            <div className="best-pro slider-items-products container">
              <div className="new_title">
                <h2>Best Seller</h2>
                <h4>So you get to know me better</h4>
              </div>
              <BestSeller bestSellers={this.props.bestSellers} />
            </div>
          </section>
          <div className="hot-section">
            <div className="container">
              <div className="row">
                <div className="ad-info">
                  <h2>Hurry Up!</h2>
                  <h3>Deal of the week</h3>
                  <h4>From our family farm right to your doorstep.</h4>
                </div>
              </div>
              <div className="row">
                <div className="hot-deal">
                  <div className="box-timer">
                    <div className="countbox_1 timer-grid"></div>
                  </div>
                  <DealOfWeek dealOfWeeks={this.props.dealOfWeeks} />
                </div>
              </div>
              <div className="row">
                <div className="testimonials-section slider-items-products">
                  <TestimonialCarousel comments={this.props.comments} />
                </div>
              </div>
              <div className="row">
                <div className="logo-brand">
                  <div className="slider-items-products">
                    <BrandCarousel />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Blog blogs={this.props.blogs} />
        </div>
        <div className="mid-section">
          <div className="container">
            <div className="row">
              <h3>Fresh organic foods delivery made easy</h3>
              <h2>Special Product</h2>
            </div>
            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div className="block1">
                  {" "}
                  <strong>fresh from our farm</strong>
                  <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy habitant morbi.
                  </p>
                </div>
                <div className="block2">
                  {" "}
                  <strong>100% organic Foods</strong>
                  <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy habitant morbi.
                  </p>
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div className="spl-pro">
                  <Link
                    to={"product-details"}
                    title="Fresh Organic Mustard Leaves "
                  >
                    <img
                      src="/assets/img/p12.jpg"
                      alt="Fresh Organic Mustard Leaves "
                    />
                  </Link>
                  <div className="item-info">
                    <div className="info-inner">
                      <div className="item-title">
                        <Link
                          to={"product-details"}
                          title="Fresh Organic Mustard Leaves "
                        >
                          Fresh Organic Mustard Leaves{" "}
                        </Link>{" "}
                      </div>
                      <div className="item-content">
                        <div className="rating">
                          <div className="ratings">
                            <div className="rating-box">
                              <div className="rating"></div>
                            </div>
                            <p className="rating-links">
                              <Link to={" "}>1 Review(s)</Link>{" "}
                              <span className="separator">|</span>{" "}
                              <Link to={" "}>Add Review</Link>{" "}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div className="block3">
                  {" "}
                  <strong>Good for health</strong>
                  <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy habitant morbi.
                  </p>
                </div>
                <div className="block4">
                  {" "}
                  <strong>Safe From Pesticides</strong>
                  <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy habitant morbi.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row our-features-box">
            <ul>
              <li>
                <div className="feature-box">
                  <div className="icon-truck"></div>
                  <div className="content">FREE SHIPPING on order over $99</div>
                </div>
              </li>
              <li>
                <div className="feature-box">
                  <div className="icon-support"></div>
                  <div className="content">
                    Have a question? <br />
                    +1 800 789 0000{" "}
                  </div>
                </div>
              </li>
              <li>
                <div className="feature-box">
                  <div className="icon-money"></div>
                  <div className="content">100% Money Back Guarantee</div>
                </div>
              </li>
              <li>
                <div className="feature-box">
                  <div className="icon-return"></div>
                  <div className="content">30 days return Service</div>
                </div>
              </li>
              <li className="last">
                <div className="feature-box">
                  {" "}
                  <Link to={" "}>
                    <i className="fa fa-apple"></i> download
                  </Link>{" "}
                  <Link to={" "}>
                    <i className="fa fa-android"></i> download
                  </Link>{" "}
                </div>
              </li>
            </ul>
          </div>
        </div>
        {/* <!-- For version 1,2,3,4,6 --></div> */}
      </>
    );
  }
}
export default Index;
