import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { name } from "../reducers";
import { withRouter } from "react-router-dom";
// import { LazyLoadImage } from "react-lazy-load-image-component";
import { get, isEmpty } from "lodash";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 10,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 8,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 4,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,
  },
};

function ToptopCategories(props) {
  const { topCategories } = props;
  if (isEmpty(topCategories)) return null;
  return (
    <Fragment>
      <div id="top-topCategories" className="product-flexslider hidden-buttons">
        <Carousel responsive={responsive}>
          {topCategories &&
            topCategories.map((o, i) => (
              <div className="item" key={i}>
                <Link to={`/san-pham/${o._id}`}>
                  <div className="pro-img">
                    <img
                      src={get(o, "cover", "/assets/img/p1.jpg")}
                      effect="blur"
                      alt={get(o, "name")}
                    />
                    <div className="pro-info">{get(o, "name")}</div>
                  </div>
                </Link>
              </div>
            ))}
        </Carousel>
      </div>
    </Fragment>
  );
}

export default ToptopCategories;
