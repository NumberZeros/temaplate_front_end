import React, { PureComponent } from "react";
import { name } from "../reducers";
import { connect } from "react-redux";

class BannerSlider extends PureComponent {
  render() {
    const { banners } = this.props;
    return (
      <ul>
        {banners &&
          banners.map((banner, index) => (
            <li
              data-transition="random"
              data-slotamount="7"
              data-masterspeed="1000"
              data-thumb={banner.imageLink}
              key={index}
            >
              <img
                src={banner.imageLink}
                data-bgposition="left top"
                data-bgfit="cover"
                data-bgrepeat="no-repeat"
                alt="slider-image1"
              />
              <div class="info">
                <div
                  class="tp-caption ExtraLargeTitle sft  tp-resizeme "
                  data-x="0"
                  data-y="220"
                  data-endspeed="500"
                  data-speed="500"
                  data-start="1100"
                  data-easing="Linear.easeNone"
                  data-splitin="none"
                  data-splitout="none"
                  data-elementdelay="0.1"
                  data-endelementdelay="0.1"
                  style={{ zIndex: "2", whiteSpace: "nowrap" }}
                >
                  <span>{banner.title1}</span>
                </div>
                <div
                  class="tp-caption LargeTitle sfl  tp-resizeme "
                  data-x="0"
                  data-y="300"
                  data-endspeed="500"
                  data-speed="500"
                  data-start="1300"
                  data-easing="Linear.easeNone"
                  data-splitin="none"
                  data-splitout="none"
                  data-elementdelay="0.1"
                  data-endelementdelay="0.1"
                  style={{ zIndex: "3", whiteSpace: "nowrap" }}
                >
                  <span>{banner.title2}</span>
                </div>
                <div
                  class="tp-caption sfb  tp-resizeme "
                  data-x="0"
                  data-y="520"
                  data-endspeed="500"
                  data-speed="500"
                  data-start="1500"
                  data-easing="Linear.easeNone"
                  data-splitin="none"
                  data-splitout="none"
                  data-elementdelay="0.1"
                  data-endelementdelay="0.1"
                  style={{ zIndex: "4", whiteSpace: "nowrap" }}
                >
                  <a href="#" class="buy-btn">
                    Shop Now
                  </a>
                </div>
                <div
                  class="tp-caption Title sft  tp-resizeme "
                  data-x="0"
                  data-y="420"
                  data-endspeed="500"
                  data-speed="500"
                  data-start="1500"
                  data-easing="Power2.easeInOut"
                  data-splitin="none"
                  data-splitout="none"
                  data-elementdelay="0.1"
                  data-endelementdelay="0.1"
                  style={{ zIndex: "4", whiteSpace: "nowrap" }}
                >
                  {banner.title3}
                </div>
              </div>
            </li>
          ))}
      </ul>
    );
  }
}

export default BannerSlider;
