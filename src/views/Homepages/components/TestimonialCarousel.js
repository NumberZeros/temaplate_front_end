import React, { Component } from "react";
import { Link } from "react-router-dom";

class TestimonialCarousel extends Component {
  render() {
    const { comments } = this.props;
    if (comments) {
      return (
        <>
          <div id="testimonials" className="offer-slider parallax parallax-2">
            <div className="slider-items slider-width-col6">
              {comments.map(comment => (
                <div className="item">
                  <div className="avatar">
                    <img src={comment.avatar} alt="Image" />
                  </div>
                  <div className="testimonials">{comment.content}</div>
                  <div className="clients_author">
                    {comment.name} <span>{comment.info}</span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </>
      );
    } else return null;
  }
}

export default TestimonialCarousel;
