import React, { Component } from "react";
import _ from "lodash";
import { name } from "../reducers";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class BestSeller extends Component {
  render() {
    const { bestSellers } = this.props;
    return (
      <div id="best-seller" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col4 products-grid">
          {bestSellers &&
            bestSellers.slice(0, 4).map((item, index) => (
              <div className="item" key={index}>
                <div className="item-inner">
                  <div className="item-img">
                    <div className="item-img-info">
                      <Link
                        to={`/product-details/${item._id}`}
                        title="Fresh Organic Mustard Leaves "
                        className="product-image"
                      >
                        <img
                          src={item.avatar}
                          alt="Fresh Organic Mustard Leaves "
                        />
                      </Link>
                      {item.isHot && (
                        <div className="new-label new-top-left">Hot</div>
                      )}
                      {item.sale && (
                        <div className="sale-label sale-top-left">
                          {item.sale}
                        </div>
                      )}
                      <div className="item-box-hover">
                        <div className="box-inner">
                          <div className="product-detail-bnt">
                            <Link className="button detail-bnt">
                              <span>Quick View</span>
                            </Link>
                          </div>
                          <div className="actions">
                            <span className="add-to-links">
                              <Link
                                href="#"
                                className="link-wishlist"
                                title="Add to Wishlist"
                              >
                                <span>Add to Wishlist</span>
                              </Link>{" "}
                              <Link
                                href="#"
                                className="link-compare add_to_compare"
                                title="Add to Compare"
                              >
                                <span>Add to Compare</span>
                              </Link>
                            </span>{" "}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="add_cart">
                      <button className="button btn-cart" type="button">
                        <span>Add to Cart</span>
                      </button>
                    </div>
                  </div>
                  <div className="item-info">
                    <div className="info-inner">
                      <div className="item-title">
                        <Link
                          to={"/product-details"}
                          title={_.get(item, "name", "Updating")}
                        >
                          {_.get(item, "name", "Updating")}{" "}
                        </Link>{" "}
                      </div>
                      <div className="item-content">
                        <div className="rating">
                          <div className="ratings">
                            <div className="rating-box">
                              <div className="rating"></div>
                            </div>
                            <p className="rating-links">
                              <Link href="#">1 Review(s)</Link>{" "}
                              <span className="separator">|</span>{" "}
                              <Link href="#">Add Review</Link>
                            </p>
                          </div>
                        </div>
                        <div className="item-price">
                          <div className="price-box">
                            <span className="regular-price">
                              <span className="price">
                                {_.get(item, "prices", "Updating")}
                              </span>{" "}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default BestSeller;
