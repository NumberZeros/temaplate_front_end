import React from "react";
import { Link } from "react-router-dom";

export default class Blog extends React.Component {
  render() {
    const { blogs } = this.props;
    if (blogs) {
      return (
        <div className="latest-blog wow bounceInUp animated animated container">
          <div>
            {blogs.map((blog, index) => (
              <div
                className="col-lg-6 col-md-6 col-xs-12 col-sm-6 blog-post"
                key={index}
              >
                <div className="blog_inner">
                  <div className="blog-img">
                    <Link to={"blog-detail"}>
                      <img src={blog.thumbnail} alt="blog image" />
                    </Link>
                    <div className="mask">
                      <Link className="info" to={"blog-detail"}>
                        Read More
                      </Link>
                    </div>
                  </div>
                  <div className="blog-info">
                    <div className="post-date">
                      <time
                        className="entry-date"
                        datetime={new Date(blog.date)}
                      >
                        {new Date(blog.date).getDay()}
                        <span>
                          {new Date(blog.date).toLocaleString("default", {
                            month: "short",
                          })}
                        </span>
                      </time>
                    </div>
                    <ul className="post-meta">
                      <li>
                        <i className="fa fa-user"></i>Posted by
                        <Link to={" "}> {blog.author}</Link>
                      </li>
                      <li>
                        <i className="fa fa-comments"></i>
                        <Link to={" "}>{blog.comments} comments</Link>
                      </li>
                    </ul>
                    <h3>
                      <Link to={"blog-detail"}>{blog.title}</Link>
                    </h3>
                    <p>{blog.content}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    } else return null;
  }
}
