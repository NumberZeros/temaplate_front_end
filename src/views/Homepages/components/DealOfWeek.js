import React from "react";
import _ from "lodash";
import { Link } from "react-router-dom";
import { name } from "../reducers";
import { connect } from "react-redux";

class DealOfWeek extends React.PureComponent {
  render() {
    const { dealOfWeeks } = this.props;
    return (
      <ul className="products-grid">
        {dealOfWeeks &&
          dealOfWeeks.map((deal, index) => (
            <li
              className="item col-lg-3 col-md-3 col-sm-3 col-xs-6"
              key={index}
            >
              <div className="item-inner">
                <div className="item-img">
                  <div className="item-img-info">
                    <Link
                      to={"product-details"}
                      title="Fresh Organic Mustard Leaves "
                      className="product-image"
                    >
                      <img
                        src="/assets/img/p16.jpg"
                        alt="Fresh Organic Mustard Leaves "
                      />
                    </Link>
                    {deal.isHot && (
                      <div className="new-label new-top-left">Hot</div>
                    )}
                    {deal.sale && (
                      <div className="sale-label sale-top-right">
                        {deal.sale}
                      </div>
                    )}
                    <div className="item-box-hover">
                      <div className="box-inner">
                        <div className="product-detail-bnt">
                          <Link to={" "} className="button detail-bnt">
                            <span>Quick View</span>
                          </Link>
                        </div>
                        <div className="actions">
                          <span className="add-to-links">
                            <Link
                              to={" "}
                              className="link-wishlist"
                              title="Add to Wishlist"
                            >
                              <span>Add to Wishlist</span>
                            </Link>{" "}
                            <Link
                              to={" "}
                              className="link-compare add_to_compare"
                              title="Add to Compare"
                            >
                              <span>Add to Compare</span>
                            </Link>
                          </span>{" "}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="add_cart">
                    <button className="button btn-cart" type="button">
                      <span>Add to Cart</span>
                    </button>
                  </div>
                </div>
                <div className="item-info">
                  <div className="info-inner">
                    <div className="item-title">
                      <Link
                        to={"product-details"}
                        title={_.get(deal, "name", "Updating")}
                      >
                        {_.get(deal, "name", "Updating")}{" "}
                      </Link>{" "}
                    </div>
                    <div className="item-content">
                      <div className="rating">
                        <div className="ratings">
                          <div className="rating-box">
                            <div className="rating"></div>
                          </div>
                          <p className="rating-links">
                            <Link to={" "}>1 Review(s)</Link>{" "}
                            <span className="separator">|</span>{" "}
                            <Link to={" "}>Add Review</Link>{" "}
                          </p>
                        </div>
                      </div>
                      <div className="item-price">
                        <div className="price-box">
                          <span className="regular-price">
                            <span className="price">
                              {_.get(deal, "price", "Updating")}
                            </span>
                          </span>{" "}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          ))}
      </ul>
    );
  }
}

export default DealOfWeek;
