/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "../../constants/defaultValues";

export const name = "Homepage";

const initialState = freeze({
  isLoading: false,
  banners: [
    {
      imageLink: "/assets/img/slide-img1.jpg",
      title1: "TITLE 1",
      title2: "Noi DUng title 2",
      title3: "Noi dung title 3"
    },
    {
      imageLink: "/assets/img/slide-img2.jpg",
      title1: "TITLE 1",
      title2: "Noi DUng title 2",
      title3: "Noi dung title 3"
    },
    {
      imageLink: "/assets/img/slide-img3.jpg",
      title1: "TITLE 1",
      title2: "Noi DUng title 2",
      title3: "Noi dung title 3"
    }
  ],
  topCategories: [],
  bestSellers: [
    {
      name: "Mat hang 1",
      price: 125,
      isHot: true
    },
    {
      name: "Mat hang 2",
      price: 125,
      sale: "20%"
    },
    {
      name: "Mat hang 3",
      price: 125
    },
    {
      name: "Mat hang 4",
      price: 125
    }
  ],
  dealOfWeeks: [
    { name: "Mat hang", price: "$250", isHot: true },
    { name: "Mat hang 2", price: "$150", isHot: false, sale: "20%" },
    { name: "Mat hang 3", price: "$100", isHot: false, sale: "10%" },
    { name: "Mat hang 3", price: "$100", isHot: true, sale: "10%" }
  ],
  comments: [
    {
      name: "Cong Vu",
      content: "That la mo trang web tuyet voi",
      avatar: "/assets/img/p1.jpg",
      info: "Sinh vien UIT"
    },
    {
      name: "Cong Vu",
      content: "That la mo trang web tuyet voi",
      avatar: "/assets/img/p2.jpg",
      info: "Sinh vien UIT"
    },
    {
      name: "Cong Vu",
      content: "That la mo trang web tuyet voi",
      avatar: "/assets/img/p3.jpg",
      info: "Sinh vien UIT"
    },
    {
      name: "Cong Vu",
      content: "That la mo trang web tuyet voi",
      avatar: "/assets/img/p4.jpg",
      info: "Sinh vien UIT"
    }
  ],
  blogs: [
    {
      date: new Date().getTime(),
      author: "admin",
      comments: 4,
      title: "POWERFUL AND FLEXIBLE PREMIUM ECOMMERCE THEMES",
      content:
        "Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec...",
      thumbnail: "/assets/img/banner-img1.jpg"
    },
    {
      date: new Date().getTime(),
      author: "admin",
      comments: 4,
      title: "POWERFUL AND FLEXIBLE PREMIUM ECOMMERCE THEMES",
      content:
        "Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec...",
      thumbnail: "/assets/img/banner-img1.jpg"
    }
  ]
});

export default handleActions(
  {
    [actions.getBanners]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.getBannersSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        banners: action.payload
      });
    },
    [actions.getBannersFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    [actions.getProductCategories]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.getProductCategoriesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        topCategories: action.payload
      });
    },
    [actions.getProductCategoriesFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    [actions.getBestSellers]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.getBestSellersSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        bestSellers: action.payload
      });
    },
    [actions.getBestSelelrsFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    }
  },
  initialState
);
