import { createAction } from "redux-actions";

export const getDetail = createAction("DETAIL/GET_DETAIL");
export const getDetailSuccess = createAction("DETAIL/GET_DETAIL_SUCCESS");
export const getDetailFail = createAction("DETAIL/GET_DETAIL_FAIL");
