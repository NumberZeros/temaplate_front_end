import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as action from "../actions";
import Detail from "./Detail";
import { name } from "../reducers";
import Loading from "../../../theme/common/Loading";
import { isEmpty } from "lodash";

class DetailContainer extends Component {
  componentDidMount() {
    const { match } = this.props;
    const id = match.params.id;
    this.props.actions.getDetail(id);
  }
  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <Loading />}
        <Detail {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name],
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action,
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(DetailContainer)
);
