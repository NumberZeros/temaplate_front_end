import React from "react";
import { Link } from "react-router-dom";

export default class Moreview extends React.Component {
  render() {
    return (
      <div class="more-views">
        <div class="slider-items-products">
          <div
            id="gallery_01"
            class="product-flexslider hidden-buttons product-img-thumb"
          >
            <div class="slider-items slider-width-col4 block-content">
              <div class="more-views-items">
                {" "}
                <Link to={" "} data-image="/assets/img/p2.jpg">
                  {" "}
                  <img
                    id="product-zoom0"
                    src="/assets/img/p2.jpg"
                    alt="product-image"
                  />{" "}
                </Link>
              </div>
              <div class="more-views-items">
                {" "}
                <Link to={" "} data-image="/assets/img/p3.jpg">
                  {" "}
                  <img
                    id="product-zoom1"
                    src="/assets/img/p3.jpg"
                    alt="product-image"
                  />{" "}
                </Link>
              </div>
              <div class="more-views-items">
                {" "}
                <Link to={" "} data-image="/assets/img/p4.jpg">
                  {" "}
                  <img
                    id="product-zoom2"
                    src="/assets/img/p4.jpg"
                    alt="product-image"
                  />{" "}
                </Link>
              </div>
              <div class="more-views-items">
                {" "}
                <Link to={" "} data-image="/assets/img/p14.jpg">
                  {" "}
                  <img
                    id="product-zoom3"
                    src="/assets/img/p14.jpg"
                    alt="product-image"
                  />{" "}
                </Link>{" "}
              </div>
              <div class="more-views-items">
                {" "}
                <Link to={" "} data-image="/assets/img/p6.jpg">
                  {" "}
                  <img
                    id="product-zoom4"
                    src="/assets/img/p6.jpg"
                    alt="product-image"
                  />{" "}
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
