import React, { Component } from "react";
import { Link } from "react-router-dom";
import Header from "../../../layout/header";
import Footer from "../../../layout/footer";

import Moreview from "./Moreview";
import Description from "./Description";

class ProductDetails extends Component {
  componentDidMount() {
    window.commonjs();
    window.zoomimg();
  }

  render() {
    const { detail } = this.props;
    return (
      <>
        <div class="page-heading">
          <div class="breadcrumbs">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <ul>
                    <li class="home">
                      {" "}
                      <Link to={"/"} title="Go to Home Page">
                        Home
                      </Link>{" "}
                      <span>&rsaquo;</span>{" "}
                    </li>
                    <li class="category1601">
                      {" "}
                      <strong>Vegetables</strong>{" "}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="page-title">
            <h2>Vegetables</h2>
          </div>
        </div>
        <div class="main-container col1-layout wow bounceInUp animated">
          <div class="main">
            <div class="col-main">
              <div
                class="product-view wow bounceInUp animated"
                itemscope=""
                itemtype="http://schema.org/Product"
                itemid="#product_base"
              >
                <div id="messages_product_view"></div>
                <div class="product-essential container">
                  <div class="row">
                    <form action="#" method="post" id="product_addtocart_form">
                      <div class="product-img-box col-lg-5 col-sm-5 col-xs-12">
                        <div class="new-label new-top-left">Hot</div>
                        <div class="sale-label sale-top-left">-15%</div>
                        <div class="product-image">
                          <div class="product-full">
                            {" "}
                            <img
                              id="product-zoom"
                              src={detail.avatar}
                              alt="product-image"
                            />
                          </div>
                          <Moreview />
                        </div>
                      </div>
                      <div class="product-shop col-lg- col-sm-7 col-xs-12">
                        <div class="product-next-prev">
                          {" "}
                          <Link class="product-next" to={" "}>
                            <span></span>
                          </Link>{" "}
                          <Link class="product-prev" to={" "}>
                            <span></span>
                          </Link>{" "}
                        </div>
                        <div class="brand">XPERIA</div>
                        <div class="product-name">
                          <h1>{detail.name} </h1>
                        </div>
                        <div class="ratings">
                          <div class="rating-box">
                            <div style={{ width: "60%" }} class="rating"></div>
                          </div>
                          <p class="rating-links">
                            {" "}
                            <Link to={" "}>1 Review</Link>{" "}
                            <span class="separator">|</span>{" "}
                            <Link to={" "}>Add Your Review</Link>{" "}
                          </p>
                        </div>
                        <div class="price-block">
                          <div class="price-box">
                            <p class="availability in-stock">
                              <span>In Stock</span>
                            </p>
                            <p class="special-price">
                              {" "}
                              <span class="price-label">Special Price</span>
                              <span id="product-price-48" class="price">
                                {" "}
                                ${detail.prices}{" "}
                              </span>{" "}
                            </p>
                          </div>
                        </div>
                        <div class="add-to-box">
                          <div class="add-to-cart">
                            <div class="pull-left">
                              <div class="custom pull-left">
                                <button
                                  onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty > 0 ) result.value--;return false;"
                                  class="reduced items-count"
                                  type="button"
                                >
                                  <i class="fa fa-minus">&nbsp;</i>
                                </button>
                                <input
                                  type="text"
                                  class="input-text qty"
                                  title="Qty"
                                  value="1"
                                  maxlength="12"
                                  id="qty"
                                  name="qty"
                                />
                                <button
                                  onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;"
                                  class="increase items-count"
                                  type="button"
                                >
                                  <i class="fa fa-plus">&nbsp;</i>
                                </button>
                              </div>
                            </div>
                            <button
                              onclick="productAddToCartForm.submit(this)"
                              class="button btn-cart"
                              title="Add to Cart"
                              type="button"
                            >
                              Add to Cart
                            </button>
                          </div>
                        </div>
                        <div class="short-description">
                          <div
                            dangerouslySetInnerHTML={{
                              __html: detail.description,
                            }}
                          />
                        </div>
                        <div class="email-addto-box">
                          <ul class="add-to-links">
                            <li>
                              {" "}
                              <Link class="link-wishlist" to={"/wishlist"}>
                                <span>Add to Wishlist</span>
                              </Link>
                            </li>
                            <li>
                              <span class="separator">|</span>{" "}
                              <Link class="link-compare" to={"/compare"}>
                                <span>Add to Compare</span>
                              </Link>
                            </li>
                          </ul>
                          <p class="email-friend">
                            <Link to={" "} class="">
                              <span>Email to a Friend</span>
                            </Link>
                          </p>
                        </div>
                        <div class="social">
                          <ul class="link">
                            <li class="fb">
                              <Link to={" "}></Link>
                            </li>
                            <li class="tw">
                              <Link to={" "}></Link>
                            </li>
                            <li class="googleplus">
                              <Link to={" "}></Link>
                            </li>
                            <li class="rss">
                              <Link to={" "}></Link>
                            </li>
                            <li class="pintrest">
                              <Link to={" "}></Link>
                            </li>
                            <li class="linkedin">
                              <Link to={" "}></Link>
                            </li>
                            <li class="youtube">
                              <Link to={" "}></Link>
                            </li>
                          </ul>
                        </div>

                        <ul class="shipping-pro">
                          <li>Free Wordwide Shipping</li>
                          <li>30 Days Return</li>
                          <li>Member Discount</li>
                        </ul>
                      </div>
                      {/* <!--product-shop--> */}
                      {/* <!--Detail page static block for version 3--> */}
                    </form>
                  </div>
                </div>
                {/* <!--product-essential--> */}
                <Description />
                {/* <!--product-collateral--> */}
                <div class="box-additional">
                  {/* <!-- BEGIN RELATED PRODUCTS --> */}
                  <div class="related-pro container">
                    <div class="slider-items-products">
                      <div class="new_title center">
                        <h2>Related Products</h2>
                      </div>
                      <div
                        id="related-slider"
                        class="product-flexslider hidden-buttons"
                      >
                        <div class="slider-items slider-width-col4 products-grid">
                          <div class="item">
                            <div class="item-inner">
                              <div class="item-img">
                                <div class="item-img-info">
                                  <Link
                                    to={"/product-details"}
                                    title="Fresh Organic Mustard Leaves "
                                    class="product-image"
                                  >
                                    <img
                                      src="/assets/img/p1.jpg"
                                      alt="Fresh Organic Mustard Leaves "
                                    />
                                  </Link>
                                  <div class="item-box-hover">
                                    <div class="box-inner">
                                      <div class="product-detail-bnt">
                                        <Link
                                          to={" "}
                                          class="button detail-bnt"
                                        >
                                          <span>Quick View</span>
                                        </Link>
                                      </div>
                                      <div class="actions">
                                        <span class="add-to-links">
                                          <Link
                                            to={" "}
                                            class="link-wishlist"
                                            title="Add to Wishlist"
                                          >
                                            <span>Add to Wishlist</span>
                                          </Link>{" "}
                                          <Link
                                            to={" "}
                                            class="link-compare add_to_compare"
                                            title="Add to Compare"
                                          >
                                            <span>Add to Compare</span>
                                          </Link>
                                        </span>{" "}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="add_cart">
                                  <button class="button btn-cart" type="button">
                                    <span>Add to Cart</span>
                                  </button>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <Link
                                      to={"/product-details"}
                                      title="Fresh Organic Mustard Leaves "
                                    >
                                      Fresh Organic Mustard Leaves{" "}
                                    </Link>{" "}
                                  </div>
                                  <div class="item-content">
                                    <div class="rating">
                                      <div class="ratings">
                                        <div class="rating-box">
                                          <div
                                            class="rating"
                                            style={{ width: "80%" }}
                                          ></div>
                                        </div>
                                        <p class="rating-links">
                                          <Link to={" "}>1 Review(s)</Link>{" "}
                                          <span class="separator">|</span>{" "}
                                          <Link to={" "}>Add Review</Link>{" "}
                                        </p>
                                      </div>
                                    </div>
                                    <div class="item-price">
                                      <div class="price-box">
                                        <span class="regular-price">
                                          <span class="price">$125.00</span>{" "}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          {/* <!-- Item --> */}
                          <div class="item">
                            <div class="item-inner">
                              <div class="item-img">
                                <div class="item-img-info">
                                  <Link
                                    to={"/product-details"}
                                    title="Fresh Organic Mustard Leaves "
                                    class="product-image"
                                  >
                                    <img
                                      src="/assets/img/p2.jpg"
                                      alt="Fresh Organic Mustard Leaves "
                                    />
                                  </Link>
                                  <div class="item-box-hover">
                                    <div class="box-inner">
                                      <div class="product-detail-bnt">
                                        <Link
                                          to={" "}
                                          class="button detail-bnt"
                                        >
                                          <span>Quick View</span>
                                        </Link>
                                      </div>
                                      <div class="actions">
                                        <span class="add-to-links">
                                          <Link
                                            to={" "}
                                            class="link-wishlist"
                                            title="Add to Wishlist"
                                          >
                                            <span>Add to Wishlist</span>
                                          </Link>{" "}
                                          <Link
                                            to={" "}
                                            class="link-compare add_to_compare"
                                            title="Add to Compare"
                                          >
                                            <span>Add to Compare</span>
                                          </Link>
                                        </span>{" "}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="add_cart">
                                  <button class="button btn-cart" type="button">
                                    <span>Add to Cart</span>
                                  </button>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <Link
                                      to={"/product-details"}
                                      title="Fresh Organic Mustard Leaves "
                                    >
                                      Fresh Organic Mustard Leaves{" "}
                                    </Link>{" "}
                                  </div>
                                  <div class="item-content">
                                    <div class="rating">
                                      <div class="ratings">
                                        <div class="rating-box">
                                          <div
                                            class="rating"
                                            style={{ width: "80%" }}
                                          ></div>
                                        </div>
                                        <p class="rating-links">
                                          <Link to={" "}>1 Review(s)</Link>{" "}
                                          <span class="separator">|</span>{" "}
                                          <Link to={" "}>Add Review</Link>{" "}
                                        </p>
                                      </div>
                                    </div>
                                    <div class="item-price">
                                      <div class="price-box">
                                        <span class="regular-price">
                                          <span class="price">$125.00</span>{" "}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Item --> */}

                          {/* <!-- Item --> */}
                          <div class="item">
                            <div class="item-inner">
                              <div class="item-img">
                                <div class="item-img-info">
                                  <Link
                                    to={"/product-details"}
                                    title="Fresh Organic Mustard Leaves "
                                    class="product-image"
                                  >
                                    <img
                                      src="/assets/img/p3.jpg"
                                      alt="Fresh Organic Mustard Leaves "
                                    />
                                  </Link>
                                  <div class="item-box-hover">
                                    <div class="box-inner">
                                      <div class="product-detail-bnt">
                                        <Link
                                          to={" "}
                                          class="button detail-bnt"
                                        >
                                          <span>Quick View</span>
                                        </Link>
                                      </div>
                                      <div class="actions">
                                        <span class="add-to-links">
                                          <Link
                                            to={" "}
                                            class="link-wishlist"
                                            title="Add to Wishlist"
                                          >
                                            <span>Add to Wishlist</span>
                                          </Link>{" "}
                                          <Link
                                            to={" "}
                                            class="link-compare add_to_compare"
                                            title="Add to Compare"
                                          >
                                            <span>Add to Compare</span>
                                          </Link>
                                        </span>{" "}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="add_cart">
                                  <button class="button btn-cart" type="button">
                                    <span>Add to Cart</span>
                                  </button>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <Link
                                      to={"/product-details"}
                                      title="Fresh Organic Mustard Leaves "
                                    >
                                      Fresh Organic Mustard Leaves{" "}
                                    </Link>{" "}
                                  </div>
                                  <div class="item-content">
                                    <div class="rating">
                                      <div class="ratings">
                                        <div class="rating-box">
                                          <div
                                            class="rating"
                                            style={{ width: "80%" }}
                                          ></div>
                                        </div>
                                        <p class="rating-links">
                                          <Link to={" "}>1 Review(s)</Link>{" "}
                                          <span class="separator">|</span>{" "}
                                          <Link to={" "}>Add Review</Link>{" "}
                                        </p>
                                      </div>
                                    </div>
                                    <div class="item-price">
                                      <div class="price-box">
                                        <span class="regular-price">
                                          <span class="price">$125.00</span>{" "}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Item --> */}

                          <div class="item">
                            <div class="item-inner">
                              <div class="item-img">
                                <div class="item-img-info">
                                  <Link
                                    to={"/product-details"}
                                    title="Fresh Organic Mustard Leaves "
                                    class="product-image"
                                  >
                                    <img
                                      src="/assets/img/p4.jpg"
                                      alt="Fresh Organic Mustard Leaves "
                                    />
                                  </Link>

                                  <div class="item-box-hover">
                                    <div class="box-inner">
                                      <div class="product-detail-bnt">
                                        <Link
                                          to={" "}
                                          class="button detail-bnt"
                                        >
                                          <span>Quick View</span>
                                        </Link>
                                      </div>
                                      <div class="actions">
                                        <span class="add-to-links">
                                          <Link
                                            to={" "}
                                            class="link-wishlist"
                                            title="Add to Wishlist"
                                          >
                                            <span>Add to Wishlist</span>
                                          </Link>{" "}
                                          <Link
                                            to={" "}
                                            class="link-compare add_to_compare"
                                            title="Add to Compare"
                                          >
                                            <span>Add to Compare</span>
                                          </Link>
                                        </span>{" "}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="add_cart">
                                  <button class="button btn-cart" type="button">
                                    <span>Add to Cart</span>
                                  </button>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <Link
                                      to={"/product-details"}
                                      title="Fresh Organic Mustard Leaves "
                                    >
                                      Fresh Organic Mustard Leaves{" "}
                                    </Link>{" "}
                                  </div>
                                  <div class="item-content">
                                    <div class="rating">
                                      <div class="ratings">
                                        <div class="rating-box">
                                          <div
                                            class="rating"
                                            style={{ width: "80%" }}
                                          ></div>
                                        </div>
                                        <p class="rating-links">
                                          <Link to={" "}>1 Review(s)</Link>{" "}
                                          <span class="separator">|</span>{" "}
                                          <Link to={" "}>Add Review</Link>{" "}
                                        </p>
                                      </div>
                                    </div>
                                    <div class="item-price">
                                      <div class="price-box">
                                        <span class="regular-price">
                                          <span class="price">$125.00</span>{" "}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          {/* <!-- Item --> */}
                          <div class="item">
                            <div class="item-inner">
                              <div class="item-img">
                                <div class="item-img-info">
                                  <Link
                                    to={"/product-details"}
                                    title="Fresh Organic Mustard Leaves "
                                    class="product-image"
                                  >
                                    <img
                                      src="/assets/img/p5.jpg"
                                      alt="Fresh Organic Mustard Leaves "
                                    />
                                  </Link>
                                  <div class="item-box-hover">
                                    <div class="box-inner">
                                      <div class="product-detail-bnt">
                                        <Link
                                          to={" "}
                                          class="button detail-bnt"
                                        >
                                          <span>Quick View</span>
                                        </Link>
                                      </div>
                                      <div class="actions">
                                        <span class="add-to-links">
                                          <Link
                                            to={" "}
                                            class="link-wishlist"
                                            title="Add to Wishlist"
                                          >
                                            <span>Add to Wishlist</span>
                                          </Link>{" "}
                                          <Link
                                            to={" "}
                                            class="link-compare add_to_compare"
                                            title="Add to Compare"
                                          >
                                            <span>Add to Compare</span>
                                          </Link>
                                        </span>{" "}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="add_cart">
                                  <button class="button btn-cart" type="button">
                                    <span>Add to Cart</span>
                                  </button>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <Link
                                      to={"/product-details"}
                                      title="Fresh Organic Mustard Leaves "
                                    >
                                      Fresh Organic Mustard Leaves{" "}
                                    </Link>{" "}
                                  </div>
                                  <div class="item-content">
                                    <div class="rating">
                                      <div class="ratings">
                                        <div class="rating-box">
                                          <div
                                            class="rating"
                                            style={{ width: "80%" }}
                                          ></div>
                                        </div>
                                        <p class="rating-links">
                                          <Link to={" "}>1 Review(s)</Link>{" "}
                                          <span class="separator">|</span>{" "}
                                          <Link to={" "}>Add Review</Link>{" "}
                                        </p>
                                      </div>
                                    </div>
                                    <div class="item-price">
                                      <div class="price-box">
                                        <span class="regular-price">
                                          <span class="price">$125.00</span>{" "}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Item --> */}

                          {/* <!-- Item --> */}
                          <div class="item">
                            <div class="item-inner">
                              <div class="item-img">
                                <div class="item-img-info">
                                  <Link
                                    to={"/product-details"}
                                    title="Fresh Organic Mustard Leaves "
                                    class="product-image"
                                  >
                                    <img
                                      src="/assets/img/p6.jpg"
                                      alt="Fresh Organic Mustard Leaves "
                                    />
                                  </Link>
                                  <div class="item-box-hover">
                                    <div class="box-inner">
                                      <div class="product-detail-bnt">
                                        <Link
                                          to={" "}
                                          class="button detail-bnt"
                                        >
                                          <span>Quick View</span>
                                        </Link>
                                      </div>
                                      <div class="actions">
                                        <span class="add-to-links">
                                          <Link
                                            to={" "}
                                            class="link-wishlist"
                                            title="Add to Wishlist"
                                          >
                                            <span>Add to Wishlist</span>
                                          </Link>{" "}
                                          <Link
                                            to={" "}
                                            class="link-compare add_to_compare"
                                            title="Add to Compare"
                                          >
                                            <span>Add to Compare</span>
                                          </Link>
                                        </span>{" "}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="add_cart">
                                  <button class="button btn-cart" type="button">
                                    <span>Add to Cart</span>
                                  </button>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <Link
                                      to={"/product-details"}
                                      title="Fresh Organic Mustard Leaves "
                                    >
                                      Fresh Organic Mustard Leaves{" "}
                                    </Link>{" "}
                                  </div>
                                  <div class="item-content">
                                    <div class="rating">
                                      <div class="ratings">
                                        <div class="rating-box">
                                          <div
                                            class="rating"
                                            style={{ width: "80%" }}
                                          ></div>
                                        </div>
                                        <p class="rating-links">
                                          <Link to={" "}>1 Review(s)</Link>{" "}
                                          <span class="separator">|</span>{" "}
                                          <Link to={" "}>Add Review</Link>{" "}
                                        </p>
                                      </div>
                                    </div>
                                    <div class="item-price">
                                      <div class="price-box">
                                        <span class="regular-price">
                                          <span class="price">$125.00</span>{" "}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Item --> */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <!-- end related product --> */}
                </div>
                {/* <!-- end related product --> */}
              </div>
              {/* <!--box-additional--> */}
              {/* <!--product-view--> */}
            </div>
          </div>
          {/* <!--col-main--> */}
        </div>

        <div className="container">
          <div className="row our-features-box">
            <ul>
              <li>
                <div className="feature-box">
                  <div className="icon-truck"></div>
                  <div className="content">FREE SHIPPING on order over $99</div>
                </div>
              </li>
              <li>
                <div className="feature-box">
                  <div className="icon-support"></div>
                  <div className="content">
                    Have a question? <br />
                    +1 800 789 0000{" "}
                  </div>
                </div>
              </li>
              <li>
                <div className="feature-box">
                  <div className="icon-money"></div>
                  <div className="content">100% Money Back Guarantee</div>
                </div>
              </li>
              <li>
                <div className="feature-box">
                  <div className="icon-return"></div>
                  <div className="content">30 days return Service</div>
                </div>
              </li>
              <li className="last">
                <div className="feature-box">
                  {" "}
                  <Link to={" "}>
                    <i className="fa fa-apple"></i> download
                  </Link>{" "}
                  <Link to={" "}>
                    <i className="fa fa-android"></i> download
                  </Link>{" "}
                </div>
              </li>
            </ul>
          </div>
        </div>
      </>
    );
  }
}
export default ProductDetails;
