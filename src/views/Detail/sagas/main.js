import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "../../../apis/detail";
import { takeAction } from "../../../services/forkActionSagas";

export function* handleGetDetail(action) {
  try {
    const res = yield call(API.getDetail, action.payload);
    yield put(actions.getDetailSuccess(res.data));
  } catch (err) {
    yield put(actions.getDetailFail(err));
  }
}

export function* getDetail() {
  yield takeAction(actions.getDetail, handleGetDetail);
}

export default [getDetail];
