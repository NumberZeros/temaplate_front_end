/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageSize } from "../../constants/defaultValues";

export const name = "Detail";

const initialState = freeze({
  isLoading: false,
  detail: "",
});

export default handleActions(
  {
    [actions.getDetail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.getDetailSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        detail: action.payload,
      });
    },
    [actions.getDetailFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
  },
  initialState
);
