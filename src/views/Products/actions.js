/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";

export const getAllProducts = createAction("PRODUCTS/GET_ALL_PRODUCTS");
export const getAllProductsSuccess = createAction("PRODUCTS/GET_ALL_PRODUCTS_SUCCESS");
export const getAllProductsFail = createAction("PRODUCTS/GET_ALL_PRODUCTS_FAIL");