import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Products from "./Products";
import Loading from "../../../theme/common/Loading";

class ProductContainer extends Component {
  componentDidMount() {
    const { actions, pagination } = this.props;
    const { id } = this.props.match.params;
    actions.getAllProducts({ data: { _id: id, pagination } });
  }
  render() {
    const { isLoading } = this.props;
    return (
      <React.Fragment>
        {isLoading && <Loading />}
        <Products {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProductContainer)
);
