/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import { defaultPageItemSize } from "../../constants/defaultValues";

export const name = "Product";

const initialState = freeze({
  isLoading: false,
  products: [],
  pagination: {
    total: 0,
    page: 1,
    limit: defaultPageItemSize
  }
});

export default handleActions(
  {
    [actions.getAllProducts]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.getAllProductsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        products: action.payload
      });
    },
    [actions.getAllProductsFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    }
  },
  initialState
);
