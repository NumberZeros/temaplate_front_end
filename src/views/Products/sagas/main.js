import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as API from "../../../apis/products";
import { takeAction } from "../../../services/forkActionSagas";

export function* handleGetAllProducts(action) {
  try {
    const res = yield call(API.getAllProducts, action.payload.data);
    yield put(actions.getAllProductsSuccess(res.data));
  } catch (err) {
    yield put(actions.getAllProductsFail(err));
  }
}

export function* getAllProducts() {
  yield takeAction(actions.getAllProducts, handleGetAllProducts);
}

export default [getAllProducts];
