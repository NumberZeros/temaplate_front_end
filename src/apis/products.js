import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}`;

export const getAllProducts = data => {
  const endpoint = `${baseEndpoint}/productCategories/${data._id}/products`;
  return request(endpoint, "GET", data);
};
