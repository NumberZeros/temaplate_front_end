import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}`;

export const getBanners = data => {
  const endpoint = `${baseEndpoint}/banners`;
  return request(endpoint, "GET", data);
};

export const getProductCategories = data => {
  // https://staging-apis.funipos.net/pages/v1/localhost/productCategories
  const endpoint = `${baseEndpoint}/productCategories`;
  return request(endpoint, "GET", data);
};

export const getBestSellers = data => {
  const endpoint = `${baseEndpoint}/bestSeller`;
  return request(endpoint, "GET", data);
};
