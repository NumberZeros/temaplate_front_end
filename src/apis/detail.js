import config from "../config";
import { request } from "../services/api";

const baseEndpoint = `${config.apiBaseURL}`;

export const getDetail = (data) => {
  const endpoint = `${baseEndpoint}/products/${data}/`;
  return request(endpoint, "GET", data);
};
